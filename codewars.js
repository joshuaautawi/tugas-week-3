
function countPositivesSumNegatives(input) {

    let minus = 0
    let plus = 0
   
    if(input==null||input.length==0) return []
   
    else{
        let c = 0
        while(c<input.length){
            if(input[0] == 0 && input[1] == 0) return []
            input[c] > 0 ? plus++ : minus+= input[c]
            c++
        }
        return [plus,minus]
    }
    
}
console.log(countPositivesSumNegatives([0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14]))
console.log(countPositivesSumNegatives([]))